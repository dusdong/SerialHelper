﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的一般信息由以下
// 控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("实全串口调试助手")]
[assembly: AssemblyDescription("开发人员：侯连文 联系方式：QQ-896374871")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("实全软件科技有限公司")]
[assembly: AssemblyProduct("实全串口调试助手")]
[assembly: AssemblyCopyright("Copyright © 实全软件科技有限公司 2022")]
[assembly: AssemblyTrademark("实全软件科技")]
[assembly: AssemblyCulture("")]

//将 ComVisible 设置为 false 将使此程序集中的类型
//对 COM 组件不可见。  如果需要从 COM 访问此程序集中的类型，
//请将此类型的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("e13b24ae-39d6-4bc4-a452-63e399a3b01f")]

// 程序集的版本信息由下列四个值组成: 
//
//      主版本
//      次版本
//      生成号
//      修订号
//
//可以指定所有这些值，也可以使用“生成号”和“修订号”的默认值，
// 方法是按如下所示使用“*”: :
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.2022.0501")]
[assembly: AssemblyFileVersion("1.0.2022.0501")]
