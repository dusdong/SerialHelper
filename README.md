# 实全串口调试助手

#### 介绍
![输入图片说明](Document/%E5%AE%9E%E5%85%A8%E4%B8%B2%E5%8F%A3%E8%B0%83%E8%AF%95%E5%8A%A9%E6%89%8B.png)

串口调试助手，广泛应用于工控领域的数据监控、数据采集、数据分析等工作，可以帮助串口应用设计、开发、测试人员检查所开发的串口应用软硬件的数据收发状况，提高开发的速度，成为您的串口应用的开发助手。

实全串口调试助手是绿色软件，只有一个执行文件，适用于各版本Windows操作系统，基于C# .Net 4.0 框架开发。可以在一台PC上同时启动多个串口调试助手（使用不同的COM口）。

典型应用场合：通过串口调试助手与自行开发的串口程序或者串口设备进行通信联调。
支持多串口，自动监测枚举本地可用串口；自由设置串口号、波特率、校验位、数据位和停止位等（支持自定义非标准波特率）；
支持ASCII/Hex两种模式的数据收发，发送和接收的数据可以UTF-8、16进制和AscII码之间任意转换；
支持间隔发送，循环发送，批处理发送，输入数据可以从外部文件导入。


#### 软件架构
软件架构说明


#### 安装教程

绿色软件，解压运行

#### 使用说明

1. 选择串口信息，打开。
2. 设置相关接收、发送开关。
3. 发送消息，验证串口返回结果。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
